package br.javamagazine.cadastro;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public final class FeedReaderContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private FeedReaderContract() {}


    /* Inner class that defines the table contents */
    public static class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "Carros";
        public static final String COLUMN_NAME_NOME = "nome";
        public static final String ID2 = "id2";
        public static final String COLUMN_NAME_PLACA = "placa";
        public static final String COLUMN_NAME_ANO = "ano";
    }
}

