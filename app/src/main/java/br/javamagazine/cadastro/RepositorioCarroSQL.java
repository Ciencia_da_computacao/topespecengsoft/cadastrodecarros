package br.javamagazine.cadastro;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementacao "Real"
 *
 * @author Alisson
 *
 */
public class RepositorioCarroSQL implements RepositorioCarro {
    FeedReaderDbHelper dbHelper;
    public RepositorioCarroSQL(Context context) {
    //reseta o banco
//        context.deleteDatabase("FeedReader.db");

        dbHelper = new FeedReaderDbHelper(context);
        //populando inicialmente
//        salvar(new Carro("Carro 1", "ABC-1011", 2009));
//        salvar(new Carro("Carro 6", "AB611", 20085));

    }

    @Override
    public boolean salvar(Carro carro) {
        deletar(carro);

        // Gets the data repository in write mode
        System.out.println(">> SALVANDO UM CARROOOOOOOOOOOO");
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_NOME, carro.getNome());
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_ANO, carro.getAno());
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_PLACA, carro.getPlaca());

        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(FeedReaderContract.FeedEntry.TABLE_NAME, null, values);
        return newRowId != -1;

    }

    @Override
    public boolean deletar(Carro carro) {
        System.out.println("Deletando o carro com id: "+carro.getId());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // Define 'where' part of query.
        String selection = FeedReaderContract.FeedEntry.ID2 + " LIKE ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = { String.valueOf(carro.getId()) };
        // Issue SQL statement.
        int deletedRows = db.delete(FeedReaderContract.FeedEntry.TABLE_NAME, selection, selectionArgs);
        return (deletedRows > 0);
    }

    @Override
    public Carro getCarro(Long id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        System.out.println(">> getCarro\n Id="+id);

//      Define a projection that specifies which columns from the database
//      you will actually use  after this query.
        String[] projection = {
                FeedReaderContract.FeedEntry.ID2,
                FeedReaderContract.FeedEntry.COLUMN_NAME_NOME,
                FeedReaderContract.FeedEntry.COLUMN_NAME_PLACA,
                FeedReaderContract.FeedEntry.COLUMN_NAME_ANO
        };

//      Filter results WHERE "title" = 'My Title'
        String selection = FeedReaderContract.FeedEntry.ID2 + " = ?";
        String[] selectionArgs = { String.valueOf(id) };
//        String selection = "";
//        String[] selectionArgs = {  };

        Cursor cursor = db.query(
                FeedReaderContract.FeedEntry.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,
                null
        );


//        Cursor cursor = db.rawQuery("SELECT * FROM "+ FeedReaderContract.FeedEntry.TABLE_NAME + " WHERE " + FeedReaderContract.FeedEntry.ID2 + " = " + id, null);

//        Cursor cursor = db.rawQuery("SELECT * FROM " + FeedReaderContract.FeedEntry.TABLE_NAME, null);

        String nome ="";
        String placa = "";
        int ano = 0;
        System.out.println(">> carros encontrados:");
        if(cursor.moveToFirst()) {
            nome = cursor.getString(1);
            placa = cursor.getString(2);
            ano = cursor.getInt(3);
            id = cursor.getLong(0);

            System.out.println(">>" +
                    "nome: "+nome+
                    "placa: "+placa+
                    "ano: "+ano
            );
        }


        cursor.close();

        Carro carro = new Carro(nome,placa,ano);
        carro.setId(id);

        if(nome.equals(""))
            return null;
        else
            return carro;


    }

    @Override
    public List<Carro> listarCarros() {
        System.out.println("Listando todos os carrros");
        SQLiteDatabase db = dbHelper.getReadableDatabase();

//      Define a projection that specifies which columns from the database
//      you will actually use after this query.
        String[] projection = {
                FeedReaderContract.FeedEntry.ID2,
                FeedReaderContract.FeedEntry.COLUMN_NAME_NOME,
                FeedReaderContract.FeedEntry.COLUMN_NAME_PLACA,
                FeedReaderContract.FeedEntry.COLUMN_NAME_ANO
        };
        String[] selectionArgs = { };
        String selection = "";



        Cursor cursor = db.query(
                FeedReaderContract.FeedEntry.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,
                null
        );

        String nome;
        String placa;
        int ano;
        long id;
        Carro novoCarro;
        List<Carro> resposta = new ArrayList<>();

        if (cursor.moveToFirst())
        {

            do
            {
                id = cursor.getLong(0);
                nome = cursor.getString(1);
                placa = cursor.getString(2);
                ano = cursor.getInt(3);
                novoCarro = new Carro(nome,placa,ano);
                novoCarro.setId(id);
                resposta.add(novoCarro);
                System.out.println(">> Carro encontrado: "+novoCarro + " com id="+ id);

            } while (cursor.moveToNext());
        }
        cursor.close();

        return resposta;
    }

    @Override
    public Carro buscarCarroPorNome(String nome) {
        System.out.println(">> buscando por nome. Nome = "+nome);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

//      Define a projection that specifies which columns from the database
//      you will actually use after this query.
        String[] projection = {
                FeedReaderContract.FeedEntry.ID2,
                FeedReaderContract.FeedEntry.COLUMN_NAME_NOME,
                FeedReaderContract.FeedEntry.COLUMN_NAME_PLACA,
                FeedReaderContract.FeedEntry.COLUMN_NAME_ANO
        };

//      Filter results WHERE "title" = 'My Title'
        String selection = FeedReaderContract.FeedEntry.COLUMN_NAME_NOME + " = ?";
        String[] selectionArgs = { nome };

        Cursor cursor = db.query(
                FeedReaderContract.FeedEntry.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,
                null
        );

        Carro carro =  new Carro(
                cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderContract.FeedEntry.COLUMN_NAME_NOME)),
                cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderContract.FeedEntry.COLUMN_NAME_PLACA)),
                cursor.getInt(cursor.getColumnIndexOrThrow(FeedReaderContract.FeedEntry.COLUMN_NAME_ANO))
        );

//        carro.setId(id);

        return carro;
    }
}
